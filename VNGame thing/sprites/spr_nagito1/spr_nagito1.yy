{
    "id": "585d5d8c-bba1-480f-82a3-6532fba62855",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nagito1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "977489e1-d2cc-46de-9a8c-aac89f14b11b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "585d5d8c-bba1-480f-82a3-6532fba62855",
            "compositeImage": {
                "id": "3981a6bb-e5e1-4748-bd4f-1946c2bc2b8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "977489e1-d2cc-46de-9a8c-aac89f14b11b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a98c8b90-898d-4f2a-9eff-137d47978116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "977489e1-d2cc-46de-9a8c-aac89f14b11b",
                    "LayerId": "a4aa6e10-4f65-4ab4-96f1-568affc35098"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a4aa6e10-4f65-4ab4-96f1-568affc35098",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "585d5d8c-bba1-480f-82a3-6532fba62855",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}