{
    "id": "cead582e-bf6c-4b16-bf4d-819bb2941ba7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 223,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec63f3b1-7510-4797-891d-486c59b9b1cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cead582e-bf6c-4b16-bf4d-819bb2941ba7",
            "compositeImage": {
                "id": "3d173cd1-2aeb-4834-bcd4-0d1e2feae34a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec63f3b1-7510-4797-891d-486c59b9b1cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "717fc188-cb13-4fd9-85e9-38d7c7ac59e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec63f3b1-7510-4797-891d-486c59b9b1cf",
                    "LayerId": "424b11fb-33d3-4797-8952-ff8083dfa66e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "424b11fb-33d3-4797-8952-ff8083dfa66e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cead582e-bf6c-4b16-bf4d-819bb2941ba7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 224,
    "xorig": 0,
    "yorig": 0
}