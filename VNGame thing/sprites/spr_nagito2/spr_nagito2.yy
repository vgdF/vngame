{
    "id": "2d7d8804-c2e7-4b25-804a-d6ea2e9d1730",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nagito2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f381a6e-ee59-427a-ba86-62dcc782c941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d7d8804-c2e7-4b25-804a-d6ea2e9d1730",
            "compositeImage": {
                "id": "52e6eacd-bb3d-4b15-9d15-f5a47eb86ff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f381a6e-ee59-427a-ba86-62dcc782c941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98c029e0-79a8-44a0-9d1b-363ddb0c57e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f381a6e-ee59-427a-ba86-62dcc782c941",
                    "LayerId": "1ba1abc7-af4b-478e-9719-07538cb7760f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1ba1abc7-af4b-478e-9719-07538cb7760f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d7d8804-c2e7-4b25-804a-d6ea2e9d1730",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}