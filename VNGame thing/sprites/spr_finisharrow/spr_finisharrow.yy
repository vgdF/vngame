{
    "id": "475a5ffa-5314-4252-b896-f3739f4be8a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_finisharrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad23c7c8-e25c-4d79-a15d-aa2262414b24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "475a5ffa-5314-4252-b896-f3739f4be8a6",
            "compositeImage": {
                "id": "70d7b1c0-80fb-4c2b-b149-d3aca2a62f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad23c7c8-e25c-4d79-a15d-aa2262414b24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3de56e7c-188d-4439-b5cd-0c4d9b1535a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad23c7c8-e25c-4d79-a15d-aa2262414b24",
                    "LayerId": "9ac8c866-8678-46e1-ac87-f86e99ade658"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "9ac8c866-8678-46e1-ac87-f86e99ade658",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "475a5ffa-5314-4252-b896-f3739f4be8a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}