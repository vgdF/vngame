{
    "id": "c6b7f0a3-86ec-4bdb-88e7-77738a5a1126",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_namebox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35ee713c-1ac1-46df-b48f-3b8f77862bd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6b7f0a3-86ec-4bdb-88e7-77738a5a1126",
            "compositeImage": {
                "id": "780d067d-cc2d-413f-909b-a7ee3fdc2d26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35ee713c-1ac1-46df-b48f-3b8f77862bd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a0b5ad0-f98c-47d5-b8c2-a8663b590258",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ee713c-1ac1-46df-b48f-3b8f77862bd2",
                    "LayerId": "75a59fd6-4db4-4fab-88e8-c013da8e53e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "75a59fd6-4db4-4fab-88e8-c013da8e53e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6b7f0a3-86ec-4bdb-88e7-77738a5a1126",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}