{
    "id": "860814bf-89dc-4fa2-a87e-28b2799178de",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_ComicSansMS",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "93e520b9-83a3-4ce4-8eda-43317ed6b2a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c34ff75e-363e-4f5b-bb00-28d7f25db29e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 216,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "605dfd55-fc6e-43ba-8b4c-232df381347a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 208,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a9a6bd9c-f43f-4af1-8863-7ae76706e566",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 192,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c852b673-e5f0-414a-843f-04e3c8a5af82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 180,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "cf4edcd1-8524-49b0-b622-95e9ba67c325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 166,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "80a3702e-34cf-4da0-8319-a60591e749b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 154,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "39f67bc6-565f-4a50-ad6b-cb970f43a7cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 150,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2fea4a18-c93a-43ca-b1ff-b099e258f2be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 142,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5b7a182e-4e29-4746-804c-65c5c1d03207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 134,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "08e22672-c8bf-4623-b1ca-029fe693b424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 221,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e2a65642-5f90-4d71-846a-09280b37a0d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 124,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c302a7bc-9ee7-4873-81fd-c4a0bbfcdc0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 107,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "79c3d455-afc0-4f46-85d8-08cff2a7173d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 99,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c81a074f-1a4d-4a0a-92fc-e740350d2093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 94,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "dbdefe29-533a-4c6e-bad1-1d3b040b64f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 84,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2f22adac-932b-451d-83c0-61f33538afc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 72,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fbc9beb0-2f1c-4ebc-9016-9513568b674c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 64,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "085e6fa7-ef45-41f5-b9a5-ac19853d2df7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 54,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0bdc3ddc-d876-4230-b360-df8e2eddffa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 44,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e6c6cde9-c953-479d-8268-842b883812e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 32,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4b4809a4-4808-409c-862f-fce9b148865c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 112,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c45f6a6d-c3f3-4f3c-9184-3db25445a5ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 231,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5f582346-88ae-4bf8-b2a7-95b6e7ea44a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 242,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "209f76f1-b6ea-43e0-9486-bd3c4651fa66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "705a89ba-de4c-4f60-bec1-cde08eb3f237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 231,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b358106d-5275-4671-9a97-83d25ce2ebfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 226,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "3cd025f0-2653-44fa-bdc9-3ebdf9c41285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 220,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6c3e7497-3d60-4c4d-8684-ab8983d56f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 213,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6e83ac80-503d-4f7b-9333-6e190d08fafb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 204,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ecc4d96b-60e2-4990-95b9-266216752454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 196,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "653ba618-518c-46ab-a013-cb17b914ab50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 186,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "59b82287-ea4c-474a-8b41-6f7f9bea1e96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 170,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "84e20456-9277-4932-ab07-c1a26c146395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "49a9d99e-e0cc-4295-bf23-cde4bd05de3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 147,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "17eb8053-945f-46d4-996b-0cb1aa80a146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 135,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "13905c5f-afd6-4405-b6ce-06509457a2b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 123,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "58ba63a3-437d-4fdc-989c-b2a36191bf97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 112,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "11c6a53d-33e9-473d-a450-05d8e458c2af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "02c543e2-b3ff-4aae-b6ca-80daf00dfd2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 88,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e42a0e56-7ee7-4275-9520-1bf7151e19b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 75,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ba810e52-6689-4a9a-9102-eab51ee195ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 64,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4e6cccbb-ea42-4eb0-9d9d-366f051fc589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 51,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ef9120af-f89b-43c8-ae13-30bc9189bfc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 40,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "af588aa1-1670-494a-9680-9f9e4113dcd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 29,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "442e968e-1685-49a2-bc83-a01be13eaad4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 13,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "cd6b1e09-061c-4873-8ff1-d8c6835614c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 17,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "60d45e9a-791b-448d-839d-577ed5d04dfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d7b9c6b4-26e2-4d01-ab45-b108f643bfec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 237,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "eeb4f85a-ce10-4dd9-bef2-bd351c23708e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9e790e13-3784-45b4-9f76-73a533b2a822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "18e0c288-2180-48ba-8da7-44c566927266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "889d929c-0558-49a3-a948-859b59702faa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "16d6fe1c-567d-4060-99b1-3a268493814c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0c121edd-1aee-467e-82d4-f04f4eea6a39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "408edb99-f127-4fba-9ed5-e97f827a550c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "723f859a-c205-48e2-827f-091b3103fdcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ff7abb61-a0d1-479a-8cc0-9eda57db5ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b1ccf527-9972-41e3-a045-ab1b5dccd7cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3001ad96-1c05-4429-8fc2-5112bdcf92ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0b1cc56e-7f2a-4192-947b-da803b97a735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "114d85b1-002f-44d3-82b7-3040bfbe469b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "cb90315a-7857-460e-adcb-6bae3f276c73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "111e24c9-0d2e-4ef0-94ef-4038dcb0ce68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f94759a4-006b-4145-84b5-056d6d28c270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 4,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8d730448-f37b-44eb-ada9-fa6977ee4a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8547e4a0-87a2-42ec-9e01-c57dc6d1b888",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5c606dbd-2e11-4b40-8db0-8a0dd571f003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "86f68ac0-42e1-4212-951d-efebd338016d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "be195520-30f7-4f75-a5f7-ac1d00740868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3a8e12e1-850a-42db-bb0f-0afd8bae78f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "af92f0b9-3c5f-49aa-a613-378c2946ec43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 18,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9eaabb65-04ef-4e19-8f9c-a55c5a641855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 120,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6892a6c7-cfef-4cd3-b837-93f0180b3097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 28,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "127ca829-a63d-4311-a1f6-d08d743134ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 218,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "287a0eec-f9db-4d97-9248-7227e68b35cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 208,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b00e794d-fafc-4975-8942-d203d78a23d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 203,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "cdc3ebdd-6470-455a-b458-e7611f542109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 189,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "49dc9c41-ff90-42e1-847d-3add2138266d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 179,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3d573d61-5364-43c7-8405-35f6f1ce7762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 169,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8af3663e-6d3c-4030-9349-6b9e5ee910be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 159,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "44ad006e-919b-4ea0-8d85-29755255ee65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 149,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "eebb3820-134b-4725-9e5f-16fe1b3030a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 140,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9a3364be-7631-431f-a797-1c596f0d23ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 227,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8f1c8b82-de42-4b43-a7ba-3f81582efd1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 130,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3b069348-2b7f-4dd8-90bc-feac4324d841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 110,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e9593acd-db70-4123-826f-e0537f08e51d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 100,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "bba60ee3-ffa4-4ee3-b9d6-676c4c4ed7b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 87,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "09f4bc66-b652-4e30-bd42-870b2a77e057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 76,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "185e7a0f-4d78-4525-ab74-8888c362cf44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 65,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "24763ce5-cf4d-412b-9806-dab7278890b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1073d64a-2523-4990-a889-b94c1742dda1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 46,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1d60910a-6ded-4822-b613-e19da92b8d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 41,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1ce2afc7-e1b5-4e55-95a3-ef4c3381cc37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 33,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a7bd8525-4bf1-4d55-a831-66f32a5b8133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 243,
                "y": 74
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "13dc66a8-f5b4-4303-a718-34322cb9ceb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 2,
                "y": 98
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}