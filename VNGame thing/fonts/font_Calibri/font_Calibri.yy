{
    "id": "d5bf0d00-20a7-4601-9399-c70e8c466cc7",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_Calibri",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Calibri",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d856b94c-caae-4c07-b5bd-ff6402e5af44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5cfb0851-172d-4d5f-bdee-bf3ac082ea7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 2,
                "shift": 5,
                "w": 1,
                "x": 18,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "85ced351-85cb-4edb-9923-1568a1040db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 12,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c4ab19da-c9a8-46a3-8024-c761e60bbf91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f71b3794-b928-492b-a3a9-c274aeb01597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 243,
                "y": 24
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ecfef6fe-a22c-4c33-b64b-a456cd71ec4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 231,
                "y": 24
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2c9d1780-ff5d-4fa6-8fdf-84ea2b5d444c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 220,
                "y": 24
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3291aa3b-759e-4d6c-96d7-4f297b717d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 217,
                "y": 24
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c279fef7-37ae-4db6-b75d-72dd9339bed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 212,
                "y": 24
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "497c3d09-d127-41ca-a37f-a2ef7c17148f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 207,
                "y": 24
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5dd84526-119c-442c-b913-ae1f408816b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 21,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3e808eb3-52c8-4b3b-bbb5-0c38b096a1dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 198,
                "y": 24
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "57709f18-7557-4ad4-b525-24168fc9b68b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 186,
                "y": 24
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4afaf8fc-f25d-4161-acf7-fcd638ca9afe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 181,
                "y": 24
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "aef0555f-99a2-47b7-8300-0d1b37abbb43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 178,
                "y": 24
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8d748090-8b1e-4a57-a59f-eb378e9e7017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 170,
                "y": 24
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e1a9b371-4481-4cf3-b648-9c2ab01c1336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 162,
                "y": 24
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "eb573884-bb6e-430f-97a2-78cfed9b93ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 155,
                "y": 24
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "fa6bca7c-ddfc-4e1d-9c5f-d337cd6403fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 147,
                "y": 24
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b887c974-4b41-4f27-9744-1d3c6eacc7ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 139,
                "y": 24
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "44f8ca7c-26da-4bcd-ab68-0ad4c2280f72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 131,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b418a9a3-0eb0-4057-a290-8b0948fed454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 190,
                "y": 24
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f20d3e3f-c216-4c3f-95e0-4990add8df88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 28,
                "y": 46
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fe94b167-a02c-4419-8bd1-846eb6f3d106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 36,
                "y": 46
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "1a88079d-2fcb-47eb-84bd-e8600f5e5890",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 44,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2128bf73-890b-4a76-992f-4c45bda3b0b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 218,
                "y": 46
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c5ad2d7e-fdf0-4819-89c4-9d63e75ea3a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 215,
                "y": 46
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "83ec90a8-eade-4161-8e28-f6ac280f4936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 211,
                "y": 46
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3af22431-dcdf-48a2-912a-1edf26495d34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 203,
                "y": 46
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f5274b71-9553-4208-9d14-bc540c215c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 194,
                "y": 46
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8f927615-6975-4408-995e-85065b6dee6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 186,
                "y": 46
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1a15e439-f896-4bbc-b78a-1af0a9f87a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 179,
                "y": 46
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "baac6fbd-26e2-48d5-b213-421a5845465f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 165,
                "y": 46
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "bbf964e6-f411-4039-a150-cb245aa4de97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 154,
                "y": 46
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "62fd6790-0790-4b2d-a544-5a213e25cb64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 145,
                "y": 46
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "31ea7a08-e86d-499e-811b-2c9d56cb2556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 135,
                "y": 46
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bac073bd-ef1f-4463-8456-a707ec6c064b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 125,
                "y": 46
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "dd877099-1821-418f-a482-c6fc51f4e295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 117,
                "y": 46
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "eec84319-e0e1-4891-b865-1be1f3041561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 110,
                "y": 46
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2679ba78-634a-4b5c-b2ea-446159682b6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 100,
                "y": 46
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a94dac82-4a2e-4082-9c7c-de824634bbe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 90,
                "y": 46
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3cd15893-4818-4083-b34e-ee4dc83330fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 87,
                "y": 46
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "7f757562-0da1-4bb3-a68c-2f9d8f35145d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 81,
                "y": 46
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "06464d9f-23d4-4589-92da-0ed8118ea25e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 73,
                "y": 46
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0fc73f9c-6a04-405d-aad5-963f53638951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 65,
                "y": 46
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b90d6738-29f3-46f7-902e-6304c9af746c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 52,
                "y": 46
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f52eca41-9c09-4619-9d38-046c1345d222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 121,
                "y": 24
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fb207479-b2f4-432d-be53-8c606e5b58ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 110,
                "y": 24
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "31c4e17e-d35e-48fe-bddb-9d3e49398f36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 102,
                "y": 24
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "bfaa2349-3e70-483a-b235-c1328c35e119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cc612482-a1b5-43ba-8513-f014907f8ece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "68ea67a0-8a42-43e7-aad5-c84bf8917bde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "67b39391-4b38-4335-a03f-5526838c2061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "33427bd1-4af7-4e1e-92a0-3e311d2df91c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "932dede0-c3fb-43ad-8054-308cc8dae473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "34bb1f5e-4732-46ee-8cc3-16fa6739b174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4e583538-53c5-48c7-bb04-6bca6708c665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2ec4c086-7506-42e9-81cb-57e3743bf660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f975719c-ec8e-458a-8fee-b8bbb6872c27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7ef1f9ba-544c-4a52-a10b-123bfd1dd409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2df6c06a-6e84-4402-b1bb-dba6b43d4632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "bcd361c5-aa8e-4a0a-bac0-1a19bbef1141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5e3ea2ca-6f52-4c52-9621-7ed7d1d9ddd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "efa5dbb9-9ce2-4220-a68a-a0eb93931a36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f8489169-26f7-4ead-ab84-016611e348ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2df619d4-e33c-469a-9854-3b9b278a9186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "026f1c60-2cda-4002-95c2-79043a948298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a7575068-f860-4c0f-be82-ddf120ce2123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fb09d2d0-a613-4770-a2fb-37442081204d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5b404322-f448-4c6f-9e0d-d6a8323d0982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0051cb3f-70fb-441d-89aa-dc5e811e5430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4a68314b-51ea-4b63-8fe6-b0b15a160af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f3a76e56-39a9-47c2-ba10-525285c1fe15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 17,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3553ec42-1618-4897-89f6-437920a5cce4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "be943385-927e-44d1-8710-a9ce4f067a51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": -1,
                "shift": 4,
                "w": 3,
                "x": 90,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1d9b24ae-df49-4465-b2d6-a4ecf2aff1e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 83,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "05b562d8-13ea-4fe0-a334-d1b8ec9d44d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 80,
                "y": 24
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a1c60964-b5bb-479d-81f4-b9d390d7f309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 69,
                "y": 24
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a8ff10f4-7a8c-4496-a38d-db881a0e9c16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 61,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "af7e1245-3f99-4c3f-acbd-e023688cfc95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 53,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "168a90b7-c6eb-4e70-8c90-08f3b4da81fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 45,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d1973e3b-c6de-491f-aa72-5b251ecb258f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 37,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "cf251044-6991-4b7c-99df-969f39f33bfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 31,
                "y": 24
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "23f5d60a-3be2-4078-91f3-03960eb4b4d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 95,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5e64df0c-53d9-4459-b180-ebd8555900dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 25,
                "y": 24
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d36a6fa3-2dc9-4074-afca-2ae24d10fb07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 9,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0534ffb1-5b82-4ba9-b8d1-044ac690302e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "cbbcae9d-7ae0-4f2f-8104-f08d4952bcf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "05ec66a8-3f5c-4c1e-882f-8f49e32c8bb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fad816cd-0e8a-4265-8660-684098276874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4d6a8e4d-2886-407d-93d3-043987d6f775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "920e1e25-aaee-48aa-97e1-a5c61b564282",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "49b37fd8-0607-4a02-be52-70268864d5cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 3,
                "shift": 7,
                "w": 1,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e4ac49b7-d692-4ece-a72a-37a9d9f20860",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8bb4ecad-0af5-4efb-b5e9-009ed3d82066",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 226,
                "y": 46
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "36e8cdad-6493-4f58-bcad-b7c44b6fac23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 20,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 235,
                "y": 46
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "f26a121c-6f3d-449c-a697-f03da51cf0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "1f9b0c09-18bc-42fa-a8a7-ed83f41e7d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "d2955231-4e84-4a83-86e5-9f9d4354fbf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 221
        },
        {
            "id": "8dab4086-071e-4b46-b541-3cc32d1e028a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "89eb25a8-bb83-44fd-b693-e9fc8c0982e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 374
        },
        {
            "id": "1452dadb-84ca-4da7-a40e-e17c5b25c321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "7acf44b9-2709-4cce-9127-acb191e12197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "6ed3d187-dcf8-4f34-802c-8d377ce4e1c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7922
        },
        {
            "id": "eab45c8b-5a97-4430-a847-490b41964802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7924
        },
        {
            "id": "e4e970df-dc73-48d2-836a-c7108d2a320a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7926
        },
        {
            "id": "ac4bc30f-e068-4d4f-a38d-2a0903a32da7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7928
        },
        {
            "id": "23e4b1fa-dee9-42a6-a6e1-7b33785b758d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8216
        },
        {
            "id": "589804b9-f7da-4850-b922-4067ccbe1091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8220
        },
        {
            "id": "b42f9a90-d0f4-4968-8072-79d066f5806d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "966f02be-b9b9-444e-8cae-2fc9d5793161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "99c38e6e-3911-4f8e-8536-e45ac6b19254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "6f7fcfea-90a1-467e-a2d7-5e673e6b751c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "6fdcd78d-2a21-43f9-82fd-aa2e29765b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "49cfb135-8c3b-4a06-b87c-4623894f7b04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "7dbbdbd8-ee2d-4d51-a9d8-4d6abd6511cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "d94dca66-620f-4579-8996-aecab6f95f6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "cadffda2-f572-4f9e-b833-28fba2285167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "bc7c796b-01ed-48b9-a5e5-1cc78ab43e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "1991b829-d73f-4e14-94b3-baf2e0b34c43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "dba7c1cd-cbbb-4bc1-96ca-c317fd7ee36e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "b1f09f88-c0e2-4fd2-ae98-9f3f4c676995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "d62f6cbe-535f-4cc0-8f3c-fbb87b9c586f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 308
        },
        {
            "id": "36124618-276a-46a0-b7bc-cca79fd14cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "14bb9405-1de0-41eb-b78c-3231930cde02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "6c2fdacf-e48b-4063-95c4-b136464cccfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "16df6dbe-e35f-4787-a3a3-a73de6f66cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "f798ab9c-9d8f-4b06-acd7-ef012908e50c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "29247537-3beb-4dc7-abfe-2bc2334aa7d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "37b725eb-fffd-4721-b3bd-76d184d9f61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "8687c2cf-766d-4983-b57a-2e42d5313afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "bb436df5-2461-4ded-8e24-7d67af028ffa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "dbe0a751-1649-41d0-b435-c31f256e7508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "031d2816-617b-4237-811e-7334b70a1ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "e7e47398-f6fa-42ea-9f08-40093c7e73d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "7bece9a6-9782-4d4e-aa29-1fdfda9d86d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "98868f11-f90d-4065-8de6-5f86f37459e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "1e15dc8c-f41d-465e-a6a8-e620a057b075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "b3d999fa-ac05-4c78-bf78-255e4a60ea16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "9a13fc29-1951-4672-beab-0895eab37eea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "f5289b24-3ca6-4fe9-87b6-ea750b433e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "06bfb76b-9e61-40f1-820b-14adfe7e9027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "b7cd253a-ff50-4b9d-baea-397b0ddb7e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "3773bdd4-895a-418c-9f01-66c03b9dde54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "c4312c01-3f2c-4275-b9af-07961ad2ef59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "3f19e3ec-5ceb-40ae-ba58-f704246b2a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "54c4ce34-0327-4f0a-a5a6-dc08d144c081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 538
        },
        {
            "id": "da6857cf-4f3d-4bfd-bf9d-07480b1f4612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "101532b2-7864-419e-91e8-3ef5dca43c10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "bb178d38-e756-4c97-93fc-7c6653195816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "1834ddf8-5cea-40b0-a4c2-d4cb5b0b21e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "c1a107d8-7ac2-4981-a4f6-6988f088548c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7924
        },
        {
            "id": "274e34f4-3d33-4c8b-95c5-b55e6da0c8ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7926
        },
        {
            "id": "a1fdb55d-c67a-4f4e-85f1-56c9fc31b454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7928
        },
        {
            "id": "da09fb53-0b5d-426f-84fb-b51cd6560dc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "5c1e866f-bdda-4aec-b19b-1f8864024f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "943289b8-1226-4321-b6fc-15d36cae5137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        },
        {
            "id": "6bb5153a-083b-4644-af62-ada8800a156c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}