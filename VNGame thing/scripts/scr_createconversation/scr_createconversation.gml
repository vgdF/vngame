//assuming that it is called from obj_conversationmanager

var TextBox = instance_create_layer(x, y, "Instances", obj_textbox);

var arg = 0;
var i = 0;
var arg_count = argument_count;

repeat (arg_count) {
	arg[i] = argument[i];
	i++;
}

var _text = arg[0];
var _speaker;
var text_len;

if (is_array(_text)) text_len = array_length_1d(_text);	
else {
	text_len = 1;
	_text[0] = _text;
}
if (!is_array(arg[1])) _speaker = array_create(text_len, arg[1]);
else _speaker = arg[1];

#region default setup

var _effects	= array_create(text_len, [1,0]);
var _speed		= array_create(text_len, [1,0.5]);
var _textcol	= array_create(text_len, [1, global.DefaultTextColor]);
var _type		= array_create(text_len, 0);
var _nextline	= array_create(text_len, 0);
var _condition  = array_create(text_len, 0); // if something is the case
var _script		= array_create(text_len, 0);
var _sprite		= array_create(text_len, 0);
var _sound		= array_create(text_len, 0);
var _creator	= array_create(text_len, id);
var _font = array_create(text_len, 0);
#endregion

#region filling in variabless
var a;

switch(arg_count-1){
	//-1 for _sprite means that it has same sprite as last text frame
	case 11:
		a = arg[11];
		if (array_length_1d(a) != text_len) a[text_len] = 0;
		for (i = 0; i < text_len; i++) {
			_font[i] = a[i];
		}	
		
	case 10:
		a = arg[10];
		if (array_length_1d(a) != text_len) a[text_len] = 0;
		for (i = 0; i < text_len; i++) {
			_sound[i] = a[i];
		}	
		
	case 9: 
		a = arg[9]; 
		if (array_length_1d(a) != text_len) a[text_len] = 0;  
		for (i = 0; i < text_len; i++) { 
			_sprite[i] = a[i]; 
		}
		
	case 8: 
		a = arg[8];	
		if (array_length_1d(a) != text_len) a[text_len] = 0;  
		for (i = 0; i < text_len; i++) { 
			if (a[i] == -1) _textcol[i] = _textcol[i - 1];
			else _textcol[i] = a[i]; 
		}
		
	case 7: 
		a = arg[7];	
		if (array_length_1d(a) != text_len) a[text_len] = 0;  
		for (i = 0; i < text_len; i++) {
			_condition[i] = a[i]; 
		}
		
	case 6: 
		a = arg[6];	
		if (array_length_1d(a) != text_len) a[text_len] =-1; 
		for (i = 0; i < text_len; i++) {
			_script[i] = a[i]; 
		}
		
	case 5: 
		a = arg[5];	
		if (array_length_1d(a) != text_len) a[text_len] = 0; 
		for (i = 0; i < text_len; i++) { 
			_nextline[i] = a[i]; 
		}
		
	case 4: 
		a = arg[4];	
		if (array_length_1d(a) != text_len) a[text_len] = 0; 
		for (i = 0; i < text_len; i++) { 
			_type[i] = a[i]; 
		}
		
	case 3:	
	
		a = arg[3];	
		if (array_length_1d(a) != text_len) a[text_len] = 0; 
		for (i = 0; i < text_len; i++) { 
			_speed[i] = a[i]; 
		}
		
	case 2:	
		a = arg[2];	
		if (array_length_1d(a) != text_len) a[text_len] = 0;  
		for (i = 0; i < text_len; i++) { 
			_effects[i] = a[i]; 
		}
		
}
#endregion

#region moving variables to textbox object
with (TextBox) {
	text		= _text;
	creator		= _creator;
	effects		= _effects;
	text_speed	= _speed;
	type		= _type;
	nextline	= _nextline;
	executeScript = _script;
	text_col	= _textcol;
	sprites		= _sprite;
	sound		= _sound;
	speaker = _speaker;
	font = _font;
	Condition = _condition;
	i = 0;
	//repeat (text_len) {
	//	speaker[i] = _speaker[i];
	//}
	draw_set_font(font[0]);
	charSize = string_width("M");
	stringHeight = string_height("M");
	event_perform(ev_alarm, 0);	
}
#endregion


return TextBox;















