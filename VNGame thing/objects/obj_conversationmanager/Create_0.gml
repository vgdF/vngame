/// @description Insert description here
// You can write your code in this editor
x = 0;
y = 0;

global.DefaultTextColor = c_white;
text[0] = "First statement First statement";
text[1] = "Second statement";
text[2] = "Question? Question? Question? Question?"
text[3] = ["First Choice", "Second Choice", "Third Choice"];
text[4] = "Wow you chose the first choice";
text[5] = "Wow you chose the second choice";
text[6] = "Wor you chose the third choice";
text[7] = "End Conv";


speaker[0] = "Joe";
speaker[1] = "Mary";
speaker[2] = "Joe";
speaker[3] = -1;
speaker[4] = "Mary";
speaker[5] = "Mary";
speaker[6] = "Mary";
speaker[7] = "Joe";

effect[0] = [5, 3, 0]; //character #1 to character#5 using effect 3
effect[1] = 0;
effect[2] = 0;
effect[3] = [0, 0, 0];
effect[4] = 0;
effect[5] = 0;
effect[6] = 0;
effect[7] = 0;

spd[0] = 60;//[2, 12, 20, 20, 30, 20, 120];//[2, 12, 1, 120];
spd[1] = 60;
spd[2] = 60;
spd[3] = [60, 60, 60];
spd[4] = 60;
spd[5] = 60;
spd[6] = 60;
spd[7] = 60;

type[0] = 0; //0 = nonchoice
type[1] = 0;
type[2] = 0;
type[3] = 1;
type[4] = 0;
type[5] = 0;
type[6] = 0;
type[7] = 0;

nextLine[0] = 0; // 0 means that it is this line + 1 -1 means end of conversation
nextLine[1] = 0;
nextLine[2] = 0;
nextLine[3] = [4, 5, 6];
nextLine[4] = 7;
nextLine[5] = 7;
nextLine[6] = 7;
nextLine[7] = -1; // end conv

Script[0] = -1;
Script[1] = -1;
//Script[2] = [scr_samplescript, 5]; 
//Script[2] = -1;
Script[3] = [-1, -1, -1];
Script[4] = -1;
Script[5] = -1;
Script[6] = -1;
Script[7] = -1;

Condition[0] = -1;//scr_ConditionTracker(); 
Condition[1] = -1;//[2, scr_samplecondition, 8]; // format [Line to go to if condition is not fufilled, Script where condition is being held, Condition arguments]
Condition[2] = -1;
Condition[3] = [-1, -1, -1];
Condition[4] = -1;
Condition[5] = -1;
Condition[6] = -1;
Condition[7] = -1;

TextColor[0] = global.DefaultTextColor;
TextColor[1] = c_orange; //-1 means same as before
TextColor[2] = -1;//[2, 5, c_red, 6, 8, c_maroon, global.DefaultTextColor];
TextColor[3] = [-1, -1, -1];
TextColor[4] = [4, 6, c_red, global.DefaultTextColor]; 
TextColor[5] = [4, 6, c_blue, 7, 9, c_orange, global.DefaultTextColor];
TextColor[6] = [4, 6, c_black, global.DefaultTextColor];
TextColor[7] = -1;

Sprites[0] = -1;
Sprites[1] = spr_nagito1;
Sprites[2] = spr_nagito2;
Sprites[3] = [spr_nagito1, spr_nagito2];
Sprites[4] = spr_nagito1;
Sprites[5] = -1;
Sprites[6] = -1; // -1 means same sprite/ sprites as before
Sprites[7] = 0; // 0 means no sprite
//have sprites fadde in and apout whent hey change

Sound[0] = -1; //[Soundfx, volume]
Sound[1] = -1;
Sound[2] = -1;//[sound_confirm, 0.1];
Sound[3] = -1;
Sound[4] = -1;
Sound[5] = -1;
Sound[6] = -1;
Sound[7] = -1;

font[0] = font_ComicSansMS;
font[1] = font_ComicSansMS;
font[2] = [0, 5, font_ComicSansMS, 7, 12, font_Calibri, font_Arial]; //first character, ending character, font for characters, font used for rest of things
font[3] = [font_ComicSansMS, font_Arial, font_Calibri];
font[4] = font_ComicSansMS;
font[5] = -1; // if -1, then it is the same as before
font[6] = -1;
font[7] = -1;
currenttextbox = scr_createconversation(text, speaker, effect, spd, type, nextLine, Script, Condition, TextColor, Sprites, Sound, font);





























