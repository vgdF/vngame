#region definitions
text = -1;
creator	= -1;
effects	= -1;
text_speed = -1;
type = -1;
nextline = -1;
executeScript = -1;
text_col = -1;
sprites = -1;
speaker = -1;
charSize = -1;
stringHeight = -1;
CurrentTextBox = -1;
Condition = -1;
pause = false;
sound = -1;
#endregion

scale = 4;

interact_key		= ord("E");
up_key				= vk_up;		
down_key			= vk_down;	

dialogue_boxSprite = spr_textbox;
name_boxSprite = spr_namebox;
finish_arrow = spr_finisharrow;

confirm_snd_effect = sound_confirm;
moveselect_snd_effect = sound_moveselect;
temp_talk_snd_effect = sound_temptalk;

text_widthbuffer = 10 * scale;
text_heightbuffer = 7 * scale;

default_col			= c_black;
choice_col			= c_yellow;
select_col			= c_orange;
name_col			= c_orange;

name_font = font_ComicSansMS;

boxHeight = sprite_get_height(dialogue_boxSprite) * scale;
boxWidth = sprite_get_width(dialogue_boxSprite) * scale;
screen_width = room_width;
screen_height = room_height;
gb_diff	= screen_width - boxWidth;
pos_x = (gb_diff / 2);
pos_y = screen_height - boxHeight - 8;


name_boxx = pos_x - (32 * scale);
name_boxy = pos_y - (16 * scale);

name_boxtextx = name_boxx + text_widthbuffer;
name_boxtexty = name_boxy + text_heightbuffer;



textFinish_x = pos_x + boxWidth - text_widthbuffer;
textFinish_y = pos_y + boxHeight - text_heightbuffer;

page = 0;