{
    "id": "2bc179f8-b36b-4563-a7a2-9aa3525caea6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_textbox",
    "eventList": [
        {
            "id": "c532ac6b-413f-40d0-9d5e-4051b745ada0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2bc179f8-b36b-4563-a7a2-9aa3525caea6"
        },
        {
            "id": "3aeeee97-c956-4960-ab79-d1524eb023ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2bc179f8-b36b-4563-a7a2-9aa3525caea6"
        },
        {
            "id": "f8550013-1c9a-4470-b70b-93b3cedefd83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2bc179f8-b36b-4563-a7a2-9aa3525caea6"
        },
        {
            "id": "7bb4345c-f642-4b15-8980-9e724d3b098a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2bc179f8-b36b-4563-a7a2-9aa3525caea6"
        },
        {
            "id": "2b777fbf-dd52-4cc9-9313-b300496b74c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "2bc179f8-b36b-4563-a7a2-9aa3525caea6"
        },
        {
            "id": "5276fa06-92c1-428b-82b4-47fa309c476b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2bc179f8-b36b-4563-a7a2-9aa3525caea6"
        },
        {
            "id": "45671057-64a2-451c-a1d6-cbb1841eefa7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2bc179f8-b36b-4563-a7a2-9aa3525caea6"
        },
        {
            "id": "56771fd4-8922-4523-b5ff-884e66976231",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "2bc179f8-b36b-4563-a7a2-9aa3525caea6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}