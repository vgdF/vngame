/// @description Insert description here
// You can write your code in this editor

draw_sprite_ext(dialogue_boxSprite, 0, pos_x,pos_y, scale,scale, 0, c_white, 1);

draw_sprite_ext(name_boxSprite, 0, name_boxx, name_boxy, scale, scale, 0, c_white, 1);	

#region drawing name
var currentname = speaker[page];
draw_set_halign(fa_center);
draw_set_font(name_font);
draw_text_color(name_boxtextx, name_boxtexty, currentname, name_col, name_col, name_col, name_col, 1);
draw_set_halign(fa_left);

#endregion
#region creating new characters


#region regular chat
if (type[page] == 0) {
	if ((charCount < CurrentStringLength) && !(pause))	{
		#region deciding text speed
		var FramesPerCharacter = text_speed[page];
		if (is_array(FramesPerCharacter)) {
			switch (array_length_1d(FramesPerCharacter)) {
				case 4:
					if (charCount >= FramesPerCharacter[0]) && (charCount <= FramesPerCharacter[1]) {
						FramesPerCharacter = FramesPerCharacter[2];
					} else {
						FramesPerCharacter = FramesPerCharacter[3];	
					}
					break;
				case 7:
					if (charCount >= FramesPerCharacter[0]) && (charCount <= FramesPerCharacter[1]) {
						FramesPerCharacter = FramesPerCharacter[2];
					} else {
						if (charCount >= FramesPerCharacter[3]) && (charCount <= FramesPerCharacter[4]) {
							FramesPerCharacter = FramesPerCharacter[5];
						} else {
							FramesPerCharacter = FramesPerCharacter[6];	
						}
					}
					break;
			}
		}
		charCount += FramesPerCharacter / room_speed;
		#endregion
		var NewCharacter = string_char_at(CurrentString, floor(charCount));	
		
		switch (NewCharacter) {
			case " ": break;
			case ",":
			case ".":
				pause = true;
				alarm[1] = 10;	//how many frames we wait if we detect a fullstop or comma
				break;
			case "?":
			case "!":
				pause = true;
				alarm[1] = 20;	//how many frames we wait if we detect a ! or ?
				break;
			default:
				var audio_increment = 2;
				if (charCount >= audio_c) {
					audio_play_sound(sound_temptalk, 1, false);
					audio_c = charCount + audio_increment;
				}
				break;
		}
	}
	var cc = 1; 
	var bp_len = -1;
	var bp_array = breakpoints;
	var next_space;
	var by = 0;
	var cy = 0;
	var cx = 0;
	var xx = pos_x + text_widthbuffer;
	var yy = pos_y + text_heightbuffer;
	var col = default_col;
	if(bp_array != -1){ 
		bp_len = array_length_1d(bp_array); 
		next_space = breakpoints[by]; 
		by++; 
	}

	repeat (floor(charCount)) {
		var letter = string_char_at(CurrentString, cc);
		if(bp_len != -1 and cc == next_space){
			cy += 1; cx = 0;
			if(by < bp_len){
				next_space = breakpoints[by];
				by++;
			}
		}
		#region set fonts
		var fontSelection = font[page]
		if (is_array(fontSelection)) {
			switch (array_length_1d(fontSelection)) {
				case 4:	
					if ((cc >= fontSelection[0]) && (cc <= fontSelection[1])) {
						draw_set_font(fontSelection[2]);
					} else {
						draw_set_font(fontSelection[3]);	
					}
					break;
				case 7:
					if (cc >= fontSelection[0]) && (cc <= fontSelection[1]) {
						draw_set_font(fontSelection[2]);
					} else {
						if (cc >= fontSelection[3]) && (cc <= fontSelection[4]) {
							draw_set_font(fontSelection[5]);
						} else {
							draw_set_font(fontSelection[6]);
						}
					}
					break;
			}
		} else {
			draw_set_font(fontSelection);	
		}
		#endregion
		#region set colors
		var colorarray = text_col[page];
		if (is_array(colorarray)) {
			switch (array_length_1d(colorarray)) {
				case 4:	
					if ((cc >= colorarray[0]) && (cc <= colorarray[1])) {
						col = colorarray[2];
					} else {
						col = colorarray[3];	
					}
					break;
				case 7:
					if (cc >= colorarray[0]) && (cc <= colorarray[1]) {
						col = colorarray[2];
					} else {
						if (cc >= colorarray[3]) && (cc <= colorarray[4]) {
							col = colorarray[5];
						} else {
							col = colorarray[6];
						}
					}
					break;
			}
		} else {
			if (colorarray != -1) col = colorarray;	
		}
		#endregion
		draw_text_color(xx + (cx * charSize), yy+(cy * stringHeight), letter, col, col, col, col, 1);
		cc++;
		cx++;
	}
		
	
	#region finished effect
	if (charCount >= CurrentStringLength) {
		draw_sprite(finish_arrow, 0, textFinish_x, textFinish_y);
	}
	#endregion
	
}
#endregion	
#region choice chat
if (type[page] == 1) {
	
}
#endregion
#endregion