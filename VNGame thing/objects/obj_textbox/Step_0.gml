
#region Normal Conversation
if (type[page] == 0) {
	if (keyboard_check_pressed(interact_key)) {
		if (charCount < CurrentStringLength){
			charCount = string_length(CurrentString);
		}
		else if (page + 1 < array_length_1d(text)) {
			event_perform(ev_other, ev_user0);
			switch(nextline[page]) {
				case -1: instance_destroy();	exit;
				case  0: page += 1;				break;
				default: page = nextline[page];
			}
			event_perform(ev_alarm, 0);
		} else {
			instance_destroy();	
		}
		
	}
}


#endregion