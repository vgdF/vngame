/// @description Check Conditions

var cv = Condition[page];

if (type[page] == 0) {
	if (is_array(cv)) { 
		var len = array_length_1d(cv) - 2 ;
		var cva = array_create(len, 0);
		array_copy(cva, 0, cv, 2, len);
		var c = cva; 
		
		var ConditionRun = scr_script_execute_alt(cv[1], cva);
		
		if (ConditionRun == false) {
			page = cv[0];
			event_perform(ev_alarm, 0);
		} 
			
		
	}
}