//new text section
#region Set Variables
charCount		= 0;
finishede_count = 0;
text_speed_c	= 0;
audio_c			= 0;
charCount_f		= 0;
effects_p		= effects[page];
text_col_p		= text_col[page];

text_speed_al	= array_length_1d(text_speed[page])/2;
effects_al		= array_length_1d(effects[page])/2;
text_col_al		= array_length_1d(text_col[page])/2;



charSize = string_width("M");		//gets new charSize under current font
charHeight = string_height("M");	//same for width
#endregion

if (type[page] == 0) {
	event_perform(ev_other, ev_user1);
	
	//put everything that happens after the check in user1
	
	#region sound playing
	var soundindex = sound[page];
 	if (is_array(soundindex)) {
		var savedvolume = audio_sound_get_gain(soundindex[0]);
		audio_sound_gain(soundindex[0], soundindex[1], 0);
		audio_play_sound(soundindex[0], 1, false);
		audio_sound_gain(soundindex[0], savedvolume, 0);
	}
	#endregion
	CurrentString = text[page];
	CurrentStringLength = string_length(CurrentString);
	
	var by = 0;
	var cc = 1;
	var next_space = 0;
	var breakpoint = 0;
	var txtwidth = boxWidth - (2 * text_widthbuffer);
	breakpoints = -1;
	repeat (CurrentStringLength) {
		var char = string_char_at(text[page], cc);
		if (cc >= next_space) {
			next_space = cc;
			while (next_space < CurrentStringLength) && (string_copy(CurrentString, next_space, 1) != " ") next_space++;
			var linewidth = (next_space - breakpoint) * charSize;
			if (linewidth >= txtwidth) {
				breakpoint = cc;
				breakpoints[by] = cc;
				by++;
			}
		}
		cc++;
	}
	
}